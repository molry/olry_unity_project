using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquidBoom : MonoBehaviour {

	public Mover mover;

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			mover.TargetPlayer (other.gameObject);
		}
	}


}